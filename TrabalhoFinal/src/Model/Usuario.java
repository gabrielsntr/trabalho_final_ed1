package Model;

public class Usuario {
    private int id;
    private String nome, senha;

    public Usuario() {
    }

    public Usuario(int id, String usuario, String senha) {
        this.id = id;
        this.nome = usuario;
        this.senha = senha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }
    
}
