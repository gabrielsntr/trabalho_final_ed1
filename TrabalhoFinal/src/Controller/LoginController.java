package Controller;

import DAO.UsuarioDAO;
import Model.Usuario;

public class LoginController {
    private UsuarioDAO usuario;

    public LoginController() {
        usuario = new UsuarioDAO();
    }
      
    public boolean login(String nome, char[] senha){
        Usuario u = usuario.selecionar(nome, new String(senha));
        if (u != null){
            return true;
        }
        return false;
    }
    
    
}
