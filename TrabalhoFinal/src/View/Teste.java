package View;

import DAO.Conexao;
import DAO.UsuarioDAO;
import Model.Usuario;
import java.sql.Connection;
import java.util.LinkedList;

public class Teste {
    static Connection con;
    static UsuarioDAO usuario = new UsuarioDAO();
    static LinkedList<Usuario> usuarios = new LinkedList();

    public static void main(String[] args) {
        usuarios = usuario.selecionar();
        for(Usuario u : usuarios){
            System.out.println(u.getNome() + " " + u.getSenha());
        }
    }
    
}
