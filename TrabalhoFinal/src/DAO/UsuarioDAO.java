package DAO;

import Model.Usuario;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.LinkedList;

public class UsuarioDAO {
    private Connection conn;
    private PreparedStatement pstm;
    private ResultSet rs;
 
    private final String SELECT_TUDO="SELECT * FROM usuario";
    private final String SELECT_ID="SELECT * FROM usuario WHERE id = ?";
    private final String SELECT_LOGIN="SELECT * FROM usuario WHERE nome = ? and senha = ?";
    private final String INSERT=" INSERT INTO usuario (nome, senha)"
            + "VALUES(?,?)";
    private final String UPDATE=" UPDATE usuario SET nome = ?, senha = ? WHERE ID = ?";
    private final String DELETE=" DELETE FROM usuario WHERE ID = ?";
    
    public LinkedList<Usuario> selecionar(){
        LinkedList<Usuario> usuarios = new LinkedList();
        Usuario info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_TUDO);
            rs=pstm.executeQuery();
            while(rs.next()){
                info = new Usuario();
                info.setId(rs.getInt("id"));
                info.setNome(rs.getString("nome"));
                info.setSenha(rs.getString("senha"));
                usuarios.add(info);
            }
 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return usuarios;
    }
    
    public Usuario selecionar(int id){
        Usuario info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_ID);
            pstm.setInt(1, id);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Usuario();
                info.setId(rs.getInt("id"));
                info.setNome(rs.getString("nome"));
                info.setSenha(rs.getString("senha"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
    }
    
    public Usuario selecionar(String nome, String senha){
        Usuario info=null;
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(SELECT_LOGIN);
            pstm.setString(1, nome);
            pstm.setString(2, senha);
            rs=pstm.executeQuery();
            while(rs.next()){
                info=new Usuario();
                info.setId(rs.getInt("id"));
                info.setNome(rs.getString("nome"));
                info.setSenha(rs.getString("senha"));
            } 
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
        return info;
    }


    public void inserir(Usuario info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(INSERT);
            pstm.setString(1, info.getNome());
            pstm.setString(2, info.getSenha());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void atualizar(Usuario info){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(UPDATE);
            pstm.setString(1, info.getNome());
            pstm.setString(2, info.getSenha());
            pstm.setInt(3, info.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
 
    public void excluir(int id){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, id);
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }
    
    public void excluir(Usuario l){
        try {
            conn=Conexao.conectar();
            pstm=conn.prepareStatement(DELETE);
            pstm.setInt(1, l.getId());
            pstm.executeUpdate();
        } catch (Exception e) {
            System.err.println("Ocorreu um erro, causa:"+e.getMessage());
            e.printStackTrace();
        }finally{
            Conexao.desconectar(conn, pstm, rs);
        }
    }

    
    
    
    
}
